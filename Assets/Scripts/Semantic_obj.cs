﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Semantic_obj : MonoBehaviour {

    public Vector2 _heightCanvas;

    public string _class;
    public double _probability;

    private Transform _cube;

    private void Start()
    {
        _cube = this.transform.Find("Cube").transform;
        Vector3 _position_canvas = transform.Find("Canvas").transform.position;
        _position_canvas.y = Random.Range(_heightCanvas.x, _heightCanvas.y);
        transform.Find("Canvas").transform.position = _position_canvas;
    }

    public void Initialize(string name, double probability)
    {
        _class = name;
        _probability = probability;

        transform.name = _class;
        gameObject.GetComponentInChildren<Text>().text = name.ToUpper() + " (" +(_probability).ToString("0.00") + ")";
    }

    public void NewPointCloud(Vector3 bb,Quaternion rotation) {
        if (_cube == null)
            _cube = this.transform.Find("Cube").transform;

        _cube.localScale = bb;
        _cube.rotation = rotation;
    }


}
