﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManaguer : MonoBehaviour {

    public CanvasGroup _uiLogging;
    public GameObject[] _enableWhenConnect;
    public GameObject[] _disableWhenConnect;

	// Use this for initialization
	void Start () {
		
	}

    void Connected() {
        Turn(_uiLogging, false);
        foreach (GameObject o in _enableWhenConnect) {
            o.SetActive(true);
        }
        foreach (GameObject o in _disableWhenConnect)
        {
            o.SetActive(false);
        }
    }

    void MapLoaded()
    {

    }

    void Turn(CanvasGroup c,bool mode) {
        c.interactable = mode;
        c.blocksRaycasts = mode;
        c.alpha = mode ? 1 : 0;
    }




}
