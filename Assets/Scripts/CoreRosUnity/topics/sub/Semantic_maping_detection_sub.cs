﻿using ROSBridgeLib;
using UnityEngine;
using SimpleJSON;
using ROSBridgeLib.semantic_maping;

public class Semantic_maping_detection_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/semantic_maping/detection";
    }

    public new static string GetMessageType()
    {
        return "semantic_maping/Detection";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new DetectionMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        Object.FindObjectOfType<Semantic_maping>().DetectedObject((DetectionMsg)msg);
    }
}
