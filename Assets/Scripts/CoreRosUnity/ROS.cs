﻿using UnityEngine;
using ROSBridgeLib;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class ROS : MonoBehaviour  {

    //public enum Packages {Trajectory,BatteryState,UserDetected };

    public bool _autoConnect;
    public Toggle _toAutoConnect;
    public Text _txVersion, _txIp;
    public string _ip = "localhost";
	public int _pot = 9090;
    public bool viewfinder = false;
    public bool _debug = false;
    public bool _conected = false;
    public Text _txPackagedEnabled;
    public List<string> _enabledPackages;
    

    private ROSBridgeWebSocketConnection _ros = null;
	private DateTime epochStart;

    private void Start()
    {
        _txVersion.text = "Version: "+Application.version;
        _autoConnect = PlayerPrefs.GetInt("autoConnect", 0) != 0;
        if(_toAutoConnect != null)
            _toAutoConnect.isOn = _autoConnect;
        //PrintPackages();
        if (_autoConnect) {
            Connect();
        }
    }

    public void ChangeAutoConnect(bool mode) {
        _autoConnect = mode;
        PlayerPrefs.SetInt("autoConnect", mode ? 1 : 0);
    }

    public void Connect() {
        if (!_conected)
        {
            epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            _ros = new ROSBridgeWebSocketConnection("ws://"+ _txIp.text, _pot);
            _ros.SetDebug(_debug);
            Debug.Log("Connecting IP:" + _txIp.text);
            _ros.Connect();
            _conected = true;
            Invoke("InitialPackage", 0.5f);
        }
    }

    void InitialPackage() {
        foreach (string type in _enabledPackages) {
            _ros.AddSubscriberOnline(Type.GetType(type));
        }


        //_ros.AddSubscriberOnline(typeof(Map_sub));
        //_ros.AddSubscriberOnline(typeof(Tf_sub));


        //if (_enabledPackages.Contains(Packages.Trajectory))
        //{
        //    _ros.AddSubscriberOnline(typeof(Move_base_global_costmap_costmap_sub),10000);
        //    _ros.AddSubscriberOnline(typeof(Move_base_local_costmap_costmap_sub),500);
        //    _ros.AddSubscriberOnline(typeof(Goal_Unity_sub));
        //    _ros.AddSubscriberOnline(typeof(Goal_sub));
        //    _ros.AddPublisherOnline(typeof(Unity_data_global_plan_pub));
        //}
        //if (_enabledPackages.Contains(Packages.BatteryState))
        //{
        //    _ros.AddSubscriberOnline(typeof(BatteryState_sub),1000);
        //}
        //if (_enabledPackages.Contains(Packages.UserDetected))
        //{
        //    _ros.AddSubscriberOnline(typeof(Users_sub),500);
        //    _ros.AddPublisherOnline(typeof(Unity_data_people_goals_pub)); 
        //}
       

        gameObject.SendMessage("Connected",SendMessageOptions.DontRequireReceiver);
    }

    public void Subcribe(Type type,int frecuency) {
        _ros.AddSubscriberOnline(type, frecuency);
    }

    public void UnSubcribe(Type unsubcribe) {
        _ros.UnSubcribe(unsubcribe);
    }

    //public void SetPackage(int package) {
    //    if (!_enabledPackages.Contains((Packages)package)) {
    //        _enabledPackages.Add((Packages)package);
    //    }
    //    PrintPackages();
    //}
    //public void DeletePackage(int package)
    //{
    //    _enabledPackages.Remove((Packages)package);
    //    PrintPackages();
    //}

    //public void PrintPackages() {
    //    string tx = "Packages enabled: \n\n";
    //    foreach (Packages p in _enabledPackages) {
    //        tx +=">"+ p.ToString() + " enabled \n";
    //    }
    //    if (_txPackagedEnabled != null)
    //        _txPackagedEnabled.text = tx;
    //    else
    //        Debug.Log(tx);
    //}

    public void Publish(String topic,ROSBridgeMsg msg)
    {
        if(!viewfinder)
            _ros.Publish(topic, msg);
    }

    // extremely important to disconnect from ROS. OTherwise packets continue to flow
    void OnApplicationQuit() {
        if (_ros != null) {
            _ros.Disconnect();
            _conected = false;
        }			
	}

	void Update () {
        if(_ros!=null)
            _ros.Render ();
	}

    public DateTime GetepochStart() { return epochStart; }
    public ROSBridgeWebSocketConnection GetCore() { return _ros; }

}