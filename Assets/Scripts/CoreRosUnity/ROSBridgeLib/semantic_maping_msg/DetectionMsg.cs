﻿using SimpleJSON;
using ROSBridgeLib.sensor_msgs;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using System;

namespace ROSBridgeLib
{
    namespace semantic_maping
    {
        public class DetectionMsg : ROSBridgeMsg
        {
            private HeaderMsg _header;
            private String _name;
            private double _probability;
            private PointCloudMsg _pointCloud;
            private PoseStampedMsg _position;
            private Vector3Msg _scale;


            public DetectionMsg(JSONNode msg)
            {
                _header = new HeaderMsg(msg["header"]);
                _name = msg["name"];
                _probability = double.Parse(msg["probability"], System.Globalization.CultureInfo.InvariantCulture);
                _pointCloud = new PointCloudMsg(msg["pointCloud"]);
                _position = new PoseStampedMsg(msg["position"]);
                _scale = new Vector3Msg(msg["scale"]);
            }

            public DetectionMsg(HeaderMsg header, String Class, float probability, PointCloudMsg pointCloud, PoseStampedMsg position, Vector3Msg scale)
            {
                _header = header;
                _name = Class;
                _probability = probability;
                _pointCloud = pointCloud;
                _position = position;
                _scale = scale;
            }

            public static string GetMessageType()
            {
                return "semantic_maping/detection";
            }

            public HeaderMsg GetHeader()
            {
                return _header;
            }
            public String GetClass()
            {
                return _name;
            }

            public double GetProbability()
            {
                return _probability;
            }

            public PointCloudMsg GetPointCloud()
            {
                return _pointCloud;
            }

            public PoseStampedMsg GetPosition() {
                return _position;
            }

            public Vector3Msg GetScale()
            {
                return _scale;
            }

            public override string ToString()
            {
                return "Detection [header=" + _header.ToString() + ", name=" + _name + ", probability="+ _probability + ", pointCloud="+ _pointCloud.ToString() + ", position=" + _position.ToString() + ", scale=" + _scale.ToString() + "]";
            }

            public override string ToYAMLString()
            {
                return "{\"header\" : " + _header.ToYAMLString() + ", \"name\" : " + _name + ", \"probability\" : " + _probability.ToString("N", System.Globalization.CultureInfo.InvariantCulture) + ", \"pointCloud\" : "+ _pointCloud.ToYAMLString() + ", \"position\" : " + _position.ToYAMLString() + ", \"scale\" : " + _scale.ToYAMLString() + "}";
            }
        }
    }
}
