﻿using UnityEngine;
using ROSBridgeLib.semantic_maping;
using ROSBridgeLib.geometry_msgs;
using System.Collections.Generic;

public class Semantic_maping : MonoBehaviour {

    public bool _modeDebug = false;
    public Transform _robotBase;
    public GameObject _detectedObjectPrefab;

    private char[] _specialChar = { '/', ' ' };

    public void DetectedObject(DetectionMsg obj) {
        
        GameObject go = GameObject.Find(obj.GetHeader().GetFrameId().Trim(_specialChar));
        if (go != null)
        {
            GameObject obj_inst = Instantiate(_detectedObjectPrefab) as GameObject;
            obj_inst.transform.parent = go.transform;
            obj_inst.transform.position = obj.GetPosition().GetPose().GetTranslationUnity();

            Semantic_obj _semantic_obj = obj_inst.GetComponent<Semantic_obj>();
            _semantic_obj.Initialize(obj.GetClass(), obj.GetProbability());
            _semantic_obj.NewPointCloud(obj.GetScale().GetVector3Unity(), obj.GetPosition().GetPose().GetRotationUnity(1));

            if (_modeDebug)
            {
                Debug.Log("Detected: "+obj.GetClass()+" -> "+obj.GetPosition() + " / "+ obj.GetScale());
            }

        }
        else {
            Debug.LogWarning("Detected object but no have a correct Frame ID");
        }

    }
}
